-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 09-09-2013 a las 19:19:56
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_elvuvm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_alumnos`
--

CREATE TABLE IF NOT EXISTS `tbl_alumnos` (
  `alumno_id` int(11) NOT NULL AUTO_INCREMENT,
  `alumno_cedula` varchar(12) NOT NULL,
  `alumno_nombres` varchar(75) DEFAULT NULL,
  `alumno_apellidos` varchar(75) DEFAULT NULL,
  `alumno_direccion` text,
  `alumno_telf` varchar(15) DEFAULT NULL,
  `alumno_email` varchar(75) DEFAULT NULL,
  `alumno_semestre` int(11) DEFAULT NULL,
  `carrera_id` int(11) NOT NULL,
  `alumno_disponibilidad` text,
  `alumno_estatus` enum('Asignado','No Asignado') NOT NULL DEFAULT 'No Asignado',
  `alumno_foto` varchar(255) DEFAULT 'alumno.png',
  `alumno_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`alumno_id`),
  KEY `carrera_id` (`carrera_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `tbl_alumnos`
--

INSERT INTO `tbl_alumnos` (`alumno_id`, `alumno_cedula`, `alumno_nombres`, `alumno_apellidos`, `alumno_direccion`, `alumno_telf`, `alumno_email`, `alumno_semestre`, `carrera_id`, `alumno_disponibilidad`, `alumno_estatus`, `alumno_foto`, `alumno_time`) VALUES
(14, '152345', 'PEDRO', 'MENDEZ', NULL, '1234455', 'PEDRO', 3, 4, NULL, 'Asignado', 'alumno.png', '2013-05-08 20:05:59'),
(15, '22622475', 'MARIA', 'MORENO', 'valera', '04267778888', 'ISA_@HOTMAIL.COM', 5, 1, 'lunes y viernes', 'Asignado', '4ebb8-IMG-20130119-00079.jpg', '2013-07-17 21:22:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_carreras`
--

CREATE TABLE IF NOT EXISTS `tbl_carreras` (
  `carrera_id` int(11) NOT NULL AUTO_INCREMENT,
  `carrera_codigo` varchar(5) NOT NULL,
  `carrera_nombre` varchar(50) NOT NULL,
  `carrera_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`carrera_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `tbl_carreras`
--

INSERT INTO `tbl_carreras` (`carrera_id`, `carrera_codigo`, `carrera_nombre`, `carrera_time`) VALUES
(1, 'IS', 'INGENIERÍA DE COMPUTACIÓN', '2013-02-27 19:16:56'),
(3, 'II', 'INGENIERÍA INDUSTRIAL', '2013-02-27 19:20:12'),
(4, 'FS', 'ADMINISTRACIÓN DE EMPRESAS', '2013-03-04 21:20:49'),
(5, 'DS', 'DERECHO', '2013-03-04 21:21:15'),
(6, 'PS', 'CIENCIAS POLÍTICAS', '2013-03-04 21:21:29'),
(7, 'CP', 'CONTADURÍA PÚBLICA', '2013-03-04 21:21:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ci_sessions`
--

CREATE TABLE IF NOT EXISTS `tbl_ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_proyectos`
--

CREATE TABLE IF NOT EXISTS `tbl_proyectos` (
  `proyecto_id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_codigo` varchar(10) NOT NULL,
  `proyecto_descripcion` varchar(150) NOT NULL,
  `proyecto_ubicacion` text NOT NULL,
  `proyecto_archivo` varchar(255) DEFAULT NULL,
  `proyecto_limite` int(11) NOT NULL DEFAULT '0',
  `proyecto_asignados` int(11) NOT NULL DEFAULT '0',
  `proyecto_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tbl_proyectos`
--

INSERT INTO `tbl_proyectos` (`proyecto_id`, `proyecto_codigo`, `proyecto_descripcion`, `proyecto_ubicacion`, `proyecto_archivo`, `proyecto_limite`, `proyecto_asignados`, `proyecto_time`) VALUES
(1, '001', 'PROYECTO 1 - LIMPIAR Y PINTAR CANCHA', 'Plata IV', NULL, 15, 1, '2013-03-31 17:35:57'),
(2, '002', 'LIMPIAR EL PARQUE ROTARI ', 'escuela de liderazgo', NULL, 15, 2, '2013-07-17 21:22:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_trabajos`
--

CREATE TABLE IF NOT EXISTS `tbl_trabajos` (
  `trabajo_id` int(11) NOT NULL AUTO_INCREMENT,
  `alumno_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `trabajo_status` enum('Abierto','Aprobado','Reprobado','Cerrado') NOT NULL DEFAULT 'Abierto',
  `trabajo_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `trabajo_fi` date NOT NULL,
  `trabajo_fc` date NOT NULL,
  PRIMARY KEY (`trabajo_id`),
  KEY `trabajo_alumno` (`alumno_id`,`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Volcado de datos para la tabla `tbl_trabajos`
--

INSERT INTO `tbl_trabajos` (`trabajo_id`, `alumno_id`, `proyecto_id`, `trabajo_status`, `trabajo_time`, `trabajo_fi`, `trabajo_fc`) VALUES
(71, 14, 2, 'Abierto', '2013-05-08 20:05:59', '2013-05-08', '2014-05-08'),
(72, 15, 2, 'Abierto', '2013-07-17 21:22:39', '2013-07-17', '2014-07-17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuario`
--

CREATE TABLE IF NOT EXISTS `tbl_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `cedula_usuario` varchar(12) NOT NULL,
  `nombre_usuario` varchar(50) DEFAULT NULL,
  `tipo_usuario` enum('Administrador','Supervisor','Usuario') NOT NULL DEFAULT 'Usuario',
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `password_confirmacion` varchar(50) NOT NULL,
  `fregistro_usuario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id_usuario`, `cedula_usuario`, `nombre_usuario`, `tipo_usuario`, `login`, `password`, `password_confirmacion`, `fregistro_usuario`) VALUES
(1, '17', 'CÉSAR ALFONSO', 'Administrador', '17', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '2012-07-23 14:49:26'),
(5, '43434343', 'USUARIO SUPERVISOR', 'Supervisor', '43434343', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '2013-03-18 17:11:49'),
(14, '152345', NULL, 'Usuario', '152345', '309b04b372507f3259411be6d88893fb', '309b04b372507f3259411be6d88893fb', '2013-05-08 20:03:08'),
(15, '22622475', 'MARIA MORENO', 'Usuario', '22622475', '73e9aeebf365180c999fc4aa7f8f9a7e', '73e9aeebf365180c999fc4aa7f8f9a7e', '2013-07-17 21:19:47');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

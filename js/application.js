$(document).ready(function(){
		 
	$("ul.submenu").parent().append("<span></span>"); 
	
	$("ul.menu li span").hover(function() { //Al hacer click se ejecuta...
		
		//Con este codigo aplicamos el movimiento de arriva y abajo para el submenu
		$(this).parent().find("ul.submenu").slideDown('fast').show(); //Menu desplegable al hacer click
 
		$(this).parent().hover(function() {
		}, function(){	
			$(this).parent().find("ul.submenu").slideUp('slow'); //Ocultamos el submenu cuando el raton sale fuera del submenu
		});
 
		}).hover(function() { 
			$(this).addClass("subhover"); //Agregamos la clase subhover
		}, function(){	//Cunado sale el cursor, sacamos la clase
			$(this).removeClass("subhover"); 
	});
	
	$('input[type="text"]').not('#field-login').not('#field-cedula_usuario').keyup(
		function(){
			valor = $(this).val();
			$(this).val(valor.toUpperCase());
	});

	$('#field-cedula_usuario').keyup(
		function(){
			valor = $(this).val();
			$('#field-login').val(valor);
	});

	$('#field-login').keyup(
		function(){
			valor = $(this).val();
			$('#field-cedula_usuario').val(valor);
	});

	// <input type="text" maxlength="50" value="123456" name="login" id="field-login">

	$('#apuntarse').click(function(){
		if (!confirm("¿Está seguro que desea apuntarse a éste proyecto?")) {
			return false;
		};
	});
	$('#mod_personales').click(function(){
		if (!confirm("¿Está seguro que desea modificar los datos?")) {
			return false;
		};
	});

	
});
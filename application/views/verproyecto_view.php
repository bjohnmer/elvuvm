<?php $this->load->view('header_view');?>

<div id="body">
	<h1 class="centrado">Escuela de Liderazgo y Valores UVM</h1>
	<p>
		<h3 class="centrado">Proyecto</h3>
		<a href="<?=base_url()?>apuntarse" class="btn btn-small btn-inverse">
			<i class="icon-arrow-left icon-white"></i> 
			Ir a la Lista
		</a>
		<p>			
		<?php if (!empty($proyecto_id)): ?>
			<?php if ($proyecto[0]->proyecto_id==$proyecto_id): ?>
					<div class="alert alert-info">
					    <button type="button" class="close" data-dismiss="alert">&times;</button>
					    <h4>¡Información!</h4>
					    Usted está apuntado a éste proyecto
				    </div>
			<?php else: ?>
				    <div class="alert alert-info">
					    <button type="button" class="close" data-dismiss="alert">&times;</button>
					    <h4>¡Información!</h4>
					    Usted ya está apuntado a otro proyecto
				    </div>
			<?php endif ?>
		<?php endif ?>
		</p>
		<p>
			<?php if (!empty($proyecto)): ?>
				    <form class="form-horizontal" action="<?=base_url()?>apuntarse/enviar" method="post">
				    	<input type="hidden" name="proyecto_id" value="<?=$proyecto[0]->proyecto_id?>">
					    <div class="control-group">
						    <label class="control-label" for="proyecto_codigo">Código</label>
						    <div class="controls">
						    	<input type="text" id="proyecto_codigo" value="<?=$proyecto[0]->proyecto_codigo?>" readonly>
						    </div>
					    </div>
					    <div class="control-group">
						    <label class="control-label" for="proyecto_descripcion">Descripción</label>
						    <div class="controls">
						    	<textarea type="text" id="proyecto_descripcion" readonly><?=$proyecto[0]->proyecto_descripcion?></textarea>
						    </div>
					    </div>					    
					    <div class="control-group">
						    <label class="control-label" for="proyecto_ubicacion">Ubicación</label>
						    <div class="controls">
						    	<textarea type="text" id="proyecto_ubicacion" readonly><?=$proyecto[0]->proyecto_ubicacion?></textarea>
						    </div>
					    </div>
						<div class="control-group">
						    <label class="control-label" for="proyecto_limite">Límite de Alumnos</label>
						    <div class="controls">
						    	<input type="text" id="proyecto_limite" value="<?=$proyecto[0]->proyecto_limite?>" readonly>
						    </div>
					    </div>
					    <?php if (!empty($proyecto[0]->proyecto_archivo)): ?>
						    <div class="control-group">
						    	<label class="control-label" for="proyecto_archivo">Ver Documento</label>
							    <div class="controls">
							    	<a id="proyecto_archivo" href="<?=base_url()?>assets/uploads/files/<?=$proyecto[0]->proyecto_archivo?>" class="btn btn-small btn-warning" target="_blank">
											<i class="icon-file icon-white"></i> 
											Abrir
									</a>
								</div>
							</div>
					    <?php endif ?>
					    <?php if (empty($proyecto_id)): ?>
							<button id="apuntarse" class="btn btn-small btn-success" >
								<i class="icon-user icon-white"></i> 
								Apuntarse
							</button>
						<?php endif ?>

				    </form>
			<?php endif ?>
		</p>
	</p>
</div>

<?php $this->load->view('footer_view');?>
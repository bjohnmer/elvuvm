<?php $this->load->view('header_view');?>

<div id="body">
	<h1 class="centrado">Escuela de Liderazgo y Valores UVM</h1>
	<p>
		<h3 class="centrado">Registro/Asignación de Trabajos Comunitarios</h3>
		<p>
		<?=$output?>
		</p>
	</p>
</div>

<?php $this->load->view('footer_view');?>

<script>
	$('#save-and-go-back-button, .btn[value=Guardar]').click(function(){
		
		code = $('#field-proyecto_id option:selected').val();

		$.post("<?=base_url()?>proyectos/test", { id: code },
		function(data){
			if (data.mensaje != 'Ok') {
				alert(data.mensaje);
				return false;
			};
		}, "json");
	});
</script>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>..:: Escuela de Liderazgo y Valores UVM ::..</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="<?=base_url()?>img/logo_thumb.png" rel="shortcut icon">
		<link href="<?=base_url()?>css/default.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="container">
			<header>
				<img border="0" src="img/elyv_cinta.png" id="membrete" />
			</header>

			<div id="body">
				<h1 class="centrado">Escuela de Liderazgo y Valores UVM</h1>
				<p>
					<h3 class="centrado">Sistema para el Registro y Control de los Trabajos Comunitarios</h3>
					<form action="<?=base_url()?>login" method="post" accept-charset="utf-8" class="entrar">
						<h4 align="center">Bienvenidos al Sistema</h4>
						<label for="login">Usuario: </label>
						<input type="text" name="login" id="login" placeholder="Ingrese Código del Usuario" size="30" />
						<label for="password">Contraseña: </label>
						<input type="password" name="password" id="password" placeholder="Ingrese Contraseña del Usuario" size="30" />	
						<input type="submit" value="Ingresar">
						<?php $this->load->view('footer_view');?>
					</form>
				</p>
			</div>
		</div>
	</body>
</html>
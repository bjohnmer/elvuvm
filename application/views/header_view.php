<!DOCTYPE html>
<html lang="es">
	<head>

		<meta charset="utf-8">
		<title>..:: Escuela de Liderazgo y Valores UVM ::..</title>
		<?php if (!empty($css_files)): ?>
			<?php foreach($css_files as $file): ?>
				<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
			<?php endforeach; ?>
		<?php endif ?>
		<?php if (!empty($js_files)): ?>
			<?php foreach($js_files as $file): ?>
				<script src="<?php echo $file; ?>"></script>
			<?php endforeach; ?>
		<?php endif ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="<?=base_url()?>img/logo_thumb.png" rel="shortcut icon" >
		<link href="<?=base_url()?>css/default.css" rel="stylesheet" type="text/css">
  		<link href="<?=base_url()?>css/admin.css" rel="stylesheet" type="text/css">
		<link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet" type="text/css" >
  	</head>
	<body>
		<div id="container">
			<header>
				<img border="0" src="<?=base_url()?>img/elyv_cinta.png" id="membrete" />
				<ul class="menu"> 
					<li><a href="<?=base_url()?>admin">Inicio</a></li> 
					<?php if ($this->session->userdata("tipo_usuario")=='Administrador') { ?>  
						<li> 
							<a href="#">Registros</a> 
						    <ul class="submenu">
						        <li><a href="<?=base_url()?>proyectos">Proyectos</a></li>
						        <li><a href="<?=base_url()?>carreras">Carreras</a></li>
								<li><a href="<?=base_url()?>alumnos">Alumnos</a></li>
						   	</ul> 
						</li>
						<li> 
							<a href="#">Procesos</a> 
						    <ul class="submenu">
						        <li><a href="<?=base_url()?>trabajos1">Asignación de Trabajos Comunitarios</a></li>
						   	</ul> 
						</li>
					<?php } else { ?>
						
						<?php if ($this->session->userdata("tipo_usuario")!='Usuario'): ?>
							<li> 
								<a href="#">Procesos</a> 
							    <ul class="submenu">
							        <li><a href="<?=base_url()?>trabajos">Asignación de Trabajos Comunitarios</a></li>
							   	</ul> 
							</li>
						<?php endif ?>

						<?php if ($this->session->userdata("tipo_usuario")=='Usuario'): ?>
						<li>
							<a href="#">Apuntarse</a> 
						    <ul class="submenu">
						        <li><a href="<?=base_url()?>apuntarse">Apuntarse a un Proyecto</a></li>
						   	</ul> 
						</li>
						<?php endif ?>
					<?php } ?>
					<li> 
						<a href="#">Configuración</a> 
					    <ul class="submenu">

					    	<?php if ($this->session->userdata("tipo_usuario")=='Administrador') { ?>           
					        	<li><a href="<?=base_url()?>usuarios">Usuarios</a></li>
					        	<li><a href="<?=base_url()?>usuarios/index/edit/<?=$this->session->userdata("id_usuario")?>">Datos del Usuario</a></li>
					        <?php } else {

					        		if ($this->session->userdata("tipo_usuario")=='Supervisor') {?>
						        		<li><a href="<?=base_url()?>datos">Datos del Usuario</a></li>			        		
									<?php } else { ?>
					        		<li><a href="<?=base_url()?>personales">Datos del Usuario</a></li>
					        	<?php } ?>
					        <?php } ?>
					   	</ul> 
					</li> 
					<li class="pull-right">
						<a href="#">Cuenta: <?php echo $this->session->userdata("nombre_usuario"); ?></a>
						<ul class="submenu">
							<li><a href="<?=base_url()?>login/logout">Cerrar Sesion</a></li>
						</ul>
					</li>
				</ul> 
				<div align="right" style="margin-right:40px;" class="usuario"><?php if ($this->session->userdata("tipo_usuario")=='Administrador') { echo "Bienvenido, ".$this->session->userdata("tipo_usuario")." del Sistema" ; } if ($this->session->userdata("tipo_usuario")=='Supervisor') { echo "Bienvenido, ".$this->session->userdata("tipo_usuario")." del Sistema"; } if ($this->session->userdata("tipo_usuario")=='Usuario') { echo "Bienvenido, ".$this->session->userdata("tipo_usuario")." del Sistema"; } ?></div>
			</header>

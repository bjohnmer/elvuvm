<?php $this->load->view('header_view');?>

<div id="body">
	<h1 class="centrado">Escuela de Liderazgo y Valores UVM</h1>
	<?php if (!empty($msn)): ?>
		<p>
			<div class="alert alert-success">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <strong>¡Bien hecho!</strong> <?=$msn?>
            </div>
		</p>
	<?php endif ?>
	<p>
		
	</p>
	<p>
		<h3 class="centrado">Trabajos Asignados</h3>
		    <form class="form-inline" action="<?=base_url()?>trabajos1/buscar" method="post">
			 	<div class="control-group">
			    	<input type="text" class="input-medium search-query" name="texto" placeholder="Texto a buscar">
				    <label class="radio">
						<input type="radio" name="campo" id="alumno_cedula" value="tbl_alumnos.alumno_cedula" checked>
						Cédula del Alumno 	 	 	 	
					</label>
					<label class="radio">
						<input type="radio" name="campo" id="proyecto_descripcion" value="tbl_proyectos.proyecto_descripcion">
						Nombre del Proyecto
					</label>
					<label class="radio">
						<input type="radio" name="campo" id="alumno_nombres" value="tbl_alumnos.alumno_nombres">
						Nombre del Alumno
					</label>
					<label class="radio">
						<input type="radio" name="campo" id="alumno_apellidos" value="tbl_alumnos.alumno_apellidos">
						Apellido del Alumno
					</label>
						&nbsp;&nbsp;&nbsp;
				    <button type="submit" class="btn btn-small btn-primary"><i class="icon-search icon-white"></i> Buscar</button>
			   	</div>
		    </form>
		    <p>
		    	<a href="<?=base_url()?>trabajos1" class="btn btn-small btn-info"><i class="icon-repeat icon-white"></i> Ver Todo</a>
		    	<a href="#" onclick="window.print(); return false;" class="btn btn-small btn-info"><i class="icon-print icon-white"></i> Imprimir</a>
		    </p>
		<p>
			<p>
				<!-- <h4>Lista de Proyectos</h4> -->
				<span class="text-info">&nbsp;&nbsp;Abierto&nbsp;&nbsp;</span>
				<span class="text-warning">&nbsp;&nbsp;Cerrado&nbsp;&nbsp;</span>
				<span class="text-success">&nbsp;&nbsp;Aprobado&nbsp;&nbsp;</span>
				<span class="text-error">&nbsp;&nbsp;Reprobado&nbsp;&nbsp;</span>
			</p>


			<ul class="media-list">
				<?php if (!empty($trabajos)): ?>
					<?php  foreach($trabajos as $row) : ?>
		        <li class="media">
		          <a href="#" class="pull-left">
		            <img class="media-object" alt="140x140" style="width: 140px; height: 140px;" src="<?=base_url()?>assets/uploads/img/<?=$row->alumno_foto?>">
		          </a>
		          <div class="media-body">
		          	<h3><?=$row->alumno_cedula?> - <?=$row->alumno_nombres?> <?=$row->alumno_apellidos?></h3>
		          	<h5><?=$row->proyecto_descripcion?> (<?=$row->proyecto_asignados?>/<?=$row->proyecto_limite?>)</h5>
		          	<p><?=$row->proyecto_ubicacion?></p>
		          	<p><strong>Inicio: </strong><?=$this->datemanager->date2normal($row->trabajo_fi)?> - <strong>Fin: </strong><?=$this->datemanager->date2normal($row->trabajo_fc)?>
		          		<div class="alert alert-<?php switch ($row->trabajo_status){
												case 'Aprobado':
													echo "success";
													break;
												case 'Abierto':
													echo "info";
													break;
												case 'Cerrado':
													echo "warning";
													break;
												case 'Reprobado':
													echo "error";
													break;
											} ?>"><?=$row->trabajo_status?>
										</div>
										<!-- <a href="<?=base_url()?>trabajos1/info/<?=$row->proyecto_id?>" class="btn btn-small btn-inverse">
											<i class="icon-folder-open icon-white"></i> 
												Ver
										</a> -->
									</p>
		          </div>
		        </li>
	        <?php endforeach; ?>
				<?php endif ?>
      </ul>
		</p>
	</p>
</div>

<?php $this->load->view('footer_view');?>
<?php $this->load->view('header_view');?>

<div id="body">
	<h1 class="centrado">Escuela de Liderazgo y Valores UVM</h1>
	<?php if (!empty($msn)): ?>
		<p>
			<div class="alert alert-success">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <strong>¡Bien hecho!</strong> <?=$msn?>
            </div>
		</p>
	<?php endif ?>
	<p>
		
	</p>
	<p>
		<h3 class="centrado">Apuntarse a un Proyecto</h3>
		    <form class="form-inline" action="<?=base_url()?>apuntarse/buscar" method="post">
			 	<div class="control-group">
			    	<input type="text" class="input-medium search-query" name="texto" placeholder="Texto a buscar">
				    <label class="radio">
						<input type="radio" name="campo" id="proyecto_codigo" value="proyecto_codigo" checked>
						Código 	 	 	 	
					</label>
					<label class="radio">
						<input type="radio" name="campo" id="proyecto_descripcion" value="proyecto_descripcion">
						Descripción
					</label>
					<label class="radio">
						<input type="radio" name="campo" id="proyecto_ubicacion" value="proyecto_ubicacion">
						Ubicación
					</label>
					<label class="radio">
						<input type="radio" name="campo" id="proyecto_limite" value="proyecto_limite">
						Límite de Alumnos
					</label>
					<label class="radio">
						<input type="radio" name="campo" id="proyecto_asignados" value="proyecto_asignados">
						Alumnos Apuntados
					</label>
			    	&nbsp;&nbsp;&nbsp;
				    <button type="submit" class="btn btn-small btn-primary"><i class="icon-search icon-white"></i> Buscar</button>
					<?php if (!empty($proyecto_id)): ?>
				    	<a href="<?=base_url()?>apuntarse/miproyecto" class="btn btn-small btn-success"><i class="icon-search icon-white"></i> Mi Proyecto</a>
					<?php endif ?>

			   	</div>
		    </form>
		    <p>
		    	<a href="<?=base_url()?>apuntarse" class="btn btn-small btn-info"><i class="icon-repeat icon-white"></i> Ver Todo</a>
		    </p>
		<p>
			<p>
				<h4>Lista de Proyectos</h4>
				<span class="text-info">&nbsp;&nbsp;Abierto&nbsp;&nbsp;</span>
				<span class="text-warning">&nbsp;&nbsp;Cerrado&nbsp;&nbsp;</span>
				<span class="text-success">&nbsp;&nbsp;Aprobado&nbsp;&nbsp;</span>
				<span class="text-error">&nbsp;&nbsp;Reprobado&nbsp;&nbsp;</span>
			</p>
			<table class="table table-hover">
				<thead>
					<tr>
						<td>Código</td>
						<td>Descripción</td>
						<td>Ubicación</td>
						<td>Límite de Alumnos</td>
						<td>Alumnos Apuntados</td>
						<td></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<?php  foreach($proyectos as $row) : ?>
					<tr <?php if (empty($apuntarse)): ?>
								<?php if (!empty($proyecto_id)): ?>
									<?php if ($row->proyecto_id==$proyecto_id): ?>
										<?php switch ($status){
											case 'Aprobado':
												echo "class='success'";
												break;
											case 'Abierto':
												echo "class='info'";
												break;
											case 'Cerrado':
												echo "class='warning'";
												break;
											case 'Reprobado':
												echo "class='error'";
												break;
										} ?>
									<?php endif ?>
								<?php endif ?>
							<?php endif ?>>
						<td>
							<?=$row->proyecto_codigo?>
						</td>
						<td>
							<?=$row->proyecto_descripcion?>
						</td>
						<td>
							<?=$row->proyecto_ubicacion?>
						</td>
						<td>
							<?=$row->proyecto_limite?>
						</td>
						<td>
							<?=$row->proyecto_asignados?>
						</td>
						<td>
							<a href="<?=base_url()?>apuntarse/info/<?=$row->proyecto_id?>" class="btn btn-small btn-inverse">
								<i class="icon-folder-open icon-white"></i> 
									Ver
							</a>
						</td>
						<td>							
							<?php if (!empty($proyecto_id)): ?>
								<?php if ($row->proyecto_id==$proyecto_id): ?>
									Apuntado
								<?php endif ?>
							<?php endif ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</p>
	</p>
</div>

<?php $this->load->view('footer_view');?>
			<p class="footer">Diseñado y Desarrollado por: <br><span class="footer_2">César Alfonso y Adriana Avendaño</span></p>
		</div>
	</body>
	<?php if (empty($js_files)): ?>
		<script type="text/javascript" src="<?=base_url()?>js/jquery-1.7.2.min.js"></script>	
	<?php endif ?>
  	<script type="text/javascript" src="<?=base_url()?>js/application.js"></script>
  	<script type="text/javascript" src="<?=base_url()?>js/bootstrap.min.js"></script>
</html>

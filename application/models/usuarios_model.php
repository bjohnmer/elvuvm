<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}

	
	public function nuevo($data)
	{	
		$usu = array(
				'cedula_usuario'		=>$data,
				'tipo_usuario'			=>'Usuario',
				'login'					=>$data,
				'password'				=>md5($data),
				'password_confirmacion'	=>md5($data)
				);
		$consulta = $this->db->insert('tbl_usuario',$usu);
		if ($consulta) {
			return true;
		} else {
			return false;
		}
	}

	public function getById($id = null)
	{
		$consulta = $this->db->where('id_usuario',$id)->get('tbl_usuario');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}	

	public function update($data)
	{
		
		$user = $this->db->where('id_usuario', $data['id_usuario'])->update('tbl_usuario', $data);
		
		if ($user) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
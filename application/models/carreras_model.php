<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Carreras_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}

	public function getAll($order = "carrera_codigo asc")
	{
		$consulta = $this->db->order_by($order)->get('tbl_carreras');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}
}
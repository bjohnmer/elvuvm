<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trabajos_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}

	public function getAll($order = "trabajo_time", $torder = "DESC")
	{
		$consulta = $this->db->join("tbl_proyectos", "tbl_proyectos.proyecto_id = tbl_trabajos.proyecto_id")->join("tbl_alumnos", "tbl_alumnos.alumno_id = tbl_trabajos.alumno_id")->order_by($order,$torder)->get('tbl_trabajos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function find($data = array())
	{
	
		switch ($data['campo'])
		{
			case 'tbl_alumnos.alumno_apellidos':
			case 'tbl_alumnos.alumno_nombres':
			case 'tbl_proyectos.proyecto_descripcion':
				$consulta = $this->db->join("tbl_proyectos", "tbl_proyectos.proyecto_id = tbl_trabajos.proyecto_id")->join("tbl_alumnos", "tbl_alumnos.alumno_id = tbl_trabajos.alumno_id")->like($data['campo'],$data['texto'])->order_by("trabajo_time","DESC")->get('tbl_trabajos');
				break;
			default:
				$consulta = $this->db->join("tbl_proyectos", "tbl_proyectos.proyecto_id = tbl_trabajos.proyecto_id")->join("tbl_alumnos", "tbl_alumnos.alumno_id = tbl_trabajos.alumno_id")->where($data['campo'],$data['texto'])->order_by("trabajo_time","DESC")->get('tbl_trabajos');
				break;
		}
		// echo $this->db->last_query();
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;

	}

	public function getById($id = null)
	{
		$consulta = $this->db->where('trabajo_id',$id)->get('tbl_trabajos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getByAlumno($id = null)
	{
		$consulta = $this->db->where('alumno_id',$id)->get('tbl_trabajos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function apuntarse($data = array(), $user = null)
	{
		$data_trabajo = array (
							'alumno_id'		=> $user,
							'proyecto_id'	=> $data['proyecto_id'],
							'trabajo_fi'	=> date('Y-m-d'),
							'trabajo_fc'	=> date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"))
						);
		$tra = $this->db->insert('tbl_trabajos', $data_trabajo);
		if ($tra) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}

	public function updateStatus()
	{
		$data_trabajo = array (
							'trabajo_status'=> 'Cerrado'
						);
		$tra = $this->db->where('trabajo_fc <',date('Y-m-d'))->where('trabajo_status','Abierto')->update('tbl_trabajos', $data_trabajo);
		if ($tra) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
	
}
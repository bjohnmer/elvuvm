<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proyectos_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}

	public function getAll($order = "proyecto_descripcion asc")
	{
		$consulta = $this->db->order_by($order)->get('tbl_proyectos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}
	public function getAllNotFull($order = "proyecto_descripcion asc")
	{
		$consulta = $this->db->where('proyecto_asignados <','proyecto_limite',FALSE)->order_by($order)->get('tbl_proyectos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}
	public function find($data = array())
	{
		switch ($data['campo'])
		{
			case 'proyecto_descripcion':
			case 'proyecto_ubicacion':
				$consulta = $this->db->like($data['campo'],$data['texto'])->get('tbl_proyectos');
				break;
			default:
				$consulta = $this->db->where($data['campo'],$data['texto'])->get('tbl_proyectos');
				break;
		}
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}

	public function getByid($id) {
		$consulta = $this->db->where("proyecto_id", $id)->get('tbl_proyectos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}

	public function restarApuntadosOnDeleteTrabajo($id)
	{
		$this->db->set('proyecto_asignados', 'proyecto_asignados-1', FALSE);
		$this->db->where('proyecto_id', $id);
		if ($this->db->update('tbl_proyectos')) {
			return true;
		} else {
			return false;
		}
		
	}

	public function sumarApuntadosOnAddTrabajo($id)
	{
		
		$this->db->set('proyecto_asignados', 'proyecto_asignados+1', FALSE);
		$this->db->where('proyecto_id', $id);
		if ($this->db->update('tbl_proyectos')) {
			return true;
		} else {
			return false;
		}
		
	}
}
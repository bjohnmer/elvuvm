<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apuntarse extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		// Carga de Librería para Manejar las Sesiones
		$this->load->library('session');

		// Verifica si hay un usuario Logueado, es decir, si hay una sesión abierta
		if (!$this->session->userdata("logged_in")) {
			// Si no es correcto, redirige al usuario hasta la página principal
			redirect('/');
		}
		// Verifica si hay un usuario Logueado, es decir, si hay una sesión abierta
		if ($this->session->userdata("tipo_usuario") == "Administrador") {
			// Si no es correcto, redirige al usuario hasta la página principal
			redirect('admin');
		}
		//fin sesion
		//Modelos
		$this->load->model('proyectos_model', 'proyectos');
		$this->load->model('alumnos_model', 'alumnos');
		$this->load->model('trabajos_model', 'trabajos');
		// Carga de librerías necesarias para manejar el módulo
		$this->load->library('Form_validation');
		// $this->load->database();	
	}
	
	public function index( $msn = null ) {
		try 
		{
				$this->trabajos->updateStatus();

				$data['proyectos'] = $this->proyectos->getAllNotFull();
				$d["alumno"] = $this->alumnos->getByCedula($this->session->userdata("cedula_usuario"));
				if (!empty($d['alumno'][0]->alumno_id)) {
					$proyecto = $this->trabajos->getByAlumno($d['alumno'][0]->alumno_id);
					if ($proyecto) {
						$data['proyecto_id'] = $proyecto[0]->proyecto_id;
						$data['status'] = $proyecto[0]->trabajo_status;
						$data['apuntarse']=false;
					}
				}
				if (!empty($msn)) {
					$data['msn'] = $msn;
				}
				$this->_example_output($data);
			
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function buscar()
	{
		try 
		{
			$this->form_validation->set_rules('texto', 'El Campo de Texto', 'required|xss_clean');
		
			if ($this->form_validation->run() == FALSE)
			{
				$this->index();
			}
			else
			{

				$respuesta = $this->proyectos->find($_POST);
				if ($respuesta) 
				{
					$data["proyectos"] = $respuesta;
					$d["alumno"] = $this->alumnos->getByCedula($this->session->userdata("cedula_usuario"));
					if (!empty($d['alumno'][0]->alumno_id)) 
					{
						$proyecto = $this->trabajos->getByAlumno($d['alumno'][0]->alumno_id);
						if ($proyecto) 
						{
							$data['proyecto_id'] = $proyecto[0]->proyecto_id;
							$data['status'] = $proyecto[0]->trabajo_status;
							$data['apuntarse']=false;
						}
					}
					$this->_example_output($data);

				} 
				else 
				{
					$this->index();
				}
			}
		}
		catch(Exception $e) 
		{
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function info()
	{
		try 
		{
			
			$respuesta = $this->proyectos->find(array('campo'=>'proyecto_id', 'texto' => $this->uri->segment(3)));
			if ($respuesta) 
			{
				$data["proyecto"] = $respuesta;
				$d["alumno"] = $this->alumnos->getByCedula($this->session->userdata("cedula_usuario"));
				if (!empty($d['alumno'][0]->alumno_id)) 
				{
					$proyecto = $this->trabajos->getByAlumno($d['alumno'][0]->alumno_id);
					if ($proyecto) 
					{
						$data['proyecto_id'] = $proyecto[0]->proyecto_id;
						$data['apuntarse']=false;
					}
				}
				$this->_example_output($data,'verproyecto_view');

			} 
			else 
			{
				$this->index();
			}
			
		}
		catch(Exception $e) 
		{
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function enviar()
	{
		try 
		{

			$alumno = $this->alumnos->getByCedula($this->session->userdata("cedula_usuario"));
			
			$this->alumnos->updateStatus($alumno[0]->alumno_id);

			$respuesta = $this->trabajos->apuntarse($_POST, $alumno[0]->alumno_id);
			if ($respuesta) 
			{

				$trabajo = $this->trabajos->getByAlumno($alumno[0]->alumno_id);
				$proyecto = $this->proyectos->getByid($trabajo[0]->proyecto_id);
				/*
					Sumar uno al proyecto
				*/
				$this->proyectos->sumarApuntadosOnAddTrabajo($proyecto[0]->proyecto_id);
				if (!empty($alumno[0]->alumno_email)) 
				{
					$nombre = $alumno[0]->alumno_nombres." ".$alumno[0]->alumno_apellidos;

					$to = $alumno[0]->alumno_email;
					
					$subject = "Apuntado al Trabajo Comunitario: ".$proyecto[0]->proyecto_descripcion;
					
					$message = '<table border="0">
								<thead>
									<tr>
										<td colspan="2"><h1>Hola '.$nombre.'</h1></td>
									</tr>
									<tr>
										<td colspan="2">
											Usted ha sido registrado correctamente en el proyecto:
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td><h3>Código:</h3></td>
										<td>&nbsp;&nbsp;'.$proyecto[0]->proyecto_codigo.'</td>
									</tr>
									<tr>
										<td><h3>Nombre:</h3></td>
										<td>&nbsp;&nbsp;'.$proyecto[0]->proyecto_descripcion.'</td>
									</tr>
									<tr>
										<td><h3>Ubicación</h3></td>
										<td>&nbsp;&nbsp;'.$proyecto[0]->proyecto_ubicacion.'</td>
									</tr>
									<tr>
										<td><h3>Fecha de inicio</h3></td>
										<td>&nbsp;&nbsp;'.$trabajo[0]->trabajo_fi.'</td>
									</tr>
									<tr>
										<td><h3>Fecha de culminación</h3></td>
										<td>&nbsp;&nbsp;'.$trabajo[0]->trabajo_fc.'</td>
									</tr>
								</tbody>
							</table>';

					$headers = "From: (sistema@gmail.com) Sistema de Consultas"."\r\n" .
					        'X-Mailer: PHP/' . phpversion() . "\r\n" .
					        "MIME-Version: 1.0\r\n" .
					        "Content-Type: text/html; charset=utf-8\r\n" .
					        "Content-Transfer-Encoding: 8bit\r\n\r\n";

					//if (mail($to, $subject, $message, $headers)) {
											
						$msn = "Usted ha sido apuntado a un Trabajo comunitario";
						$this->index($msn);
						
					//}

				}			
				else
				{

					$msn = "Usted ha sido apuntado a un Trabajo comunitario";
					$this->index($msn);

				}
			}

		}
		catch(Exception $e) 
		{
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function miproyecto()
	{
		try 
		{
			$d["alumno"] = $this->alumnos->getByCedula($this->session->userdata("cedula_usuario"));
			if (!empty($d['alumno'][0]->alumno_id)) 
			{
				
				$proyecto = $this->trabajos->getByAlumno($d['alumno'][0]->alumno_id);
				if ($proyecto) 
				{
					$data["proyectos"] = $this->proyectos->find(array('campo'=>'proyecto_id', 'texto' => $proyecto[0]->proyecto_id));
					$data['proyecto_id'] = $proyecto[0]->proyecto_id;
					$data['status'] = $proyecto[0]->trabajo_status;
					$data['apuntarse']=false;
				}
				$this->_example_output($data);
			}
			else
			{
				$this->index();
			}

			
		}
		catch(Exception $e) 
		{
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	// Función que muestra la Vista
	function _example_output($output = null,$vista='apuntarse_view') {
		$this->load->view($vista.'.php',$output);
	}
}


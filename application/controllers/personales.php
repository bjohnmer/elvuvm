<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Personales extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->model('alumnos_model', 'alumnos');
		$this->load->model('usuarios_model', 'usuarios');
		$this->load->model('carreras_model', 'carreras');

		$this->load->library('Form_validation');
	}

	public function index($data = array()) 
	{
		$data["alumno"] = $this->alumnos->getByCedula($this->session->userdata("cedula_usuario"));
		$data["carreras"] = $this->carreras->getAll();
		$this->load->view('personales_view', $data);	
	}

	public function edit() 
	{
		
		$this->form_validation->set_rules('alumno_cedula', 'Cédula', 'trim|numeric|required');
		$this->form_validation->set_rules('alumno_nombres', 'Nombres', 'trim|alpha_space|required');
		$this->form_validation->set_rules('alumno_apellidos', 'Apellidos', 'trim|alpha_space|required');
		$this->form_validation->set_rules('alumno_direccion', 'Dirección', 'required');
		$this->form_validation->set_rules('alumno_telf', 'Teléfono', 'trim|required');
		$this->form_validation->set_rules('alumno_email', 'Correo Electrónico', 'trim|valid_email|required');
		$this->form_validation->set_rules('alumno_semestre', 'Semestre', 'trim|numeric|required');
		$this->form_validation->set_rules('alumno_disponibilidad', 'Disponibilidad', 'required');

		// $this->form_validation->set_rules('password', 'Password', 'trim|matches[password_confirmacion]|required');
		// $this->form_validation->set_rules('password_confirmacion', 'Confirmar Password', 'trim|matches[password]|required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->index();
		}
		else
		{

			$usuario = array (	'id_usuario' => $this->session->userdata("id_usuario"),
								'cedula_usuario' => $_POST['alumno_cedula'],
								'nombre_usuario' => $_POST['alumno_nombres'] . " " .$_POST['alumno_apellidos'],
								'login' => $_POST['alumno_cedula']
						);

			if (!empty($_POST['password'])) {
							$usuario['password'] = md5($_POST['password']);
 $usuario['password_confirmacion'] = md5($_POST['password_confirmacion']);
			}

			$update_usuario = $this->usuarios->update($usuario);

			$al = $this->alumnos->getByCedula($this->session->userdata("cedula_usuario"));

			$alumno = array(
									'alumno_id' => $al[0]->alumno_id,
							'alumno_cedula' => $_POST['alumno_cedula'],
						 'alumno_nombres' => $_POST['alumno_nombres'],
					 'alumno_apellidos' => $_POST['alumno_apellidos'],
					 'alumno_direccion' => $_POST['alumno_direccion'],
								'alumno_telf' => $_POST['alumno_telf'],
							 'alumno_email' => $_POST['alumno_email'],
						'alumno_semestre' => $_POST['alumno_semestre'],
								 'carrera_id' => $_POST['carrera_id'],
		 'alumno_disponibilidad ' => $_POST['alumno_disponibilidad']
						);
			$update_alumno = $this->alumnos->update($alumno);

			if ($update_usuario && $update_alumno) {
				$data["mensaje"] = "Los datos se modificaron correctamente";				
			} else {
				$data['mensaje'] = 'Error: No se ha podido guardar';
			}
			$this->index($data);

		}

	}

}
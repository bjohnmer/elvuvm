<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enviar extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library("email");
	}
	
	public function index() {
		echo "fino";
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['mailtype']="html";
 
		$this->email->initialize($config);

		$this->email->from("bjohnmer@gmail.com","Sistema de Consultas");
		$this->email->to("bjohnmer@hotmail.com");
		$this->email->subject("Comentario desde el Sistema de Consultas IVSS");
		$this->email->message("Este es un comentario");
			
		$this->email->send();
		redirect('admin/');
	}
	
}
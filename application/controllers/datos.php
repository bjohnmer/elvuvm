<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datos extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->model('usuarios_model', 'usuarios');

		$this->load->library('Form_validation');
	}

	public function index($data = array()) 
	{
		$data["usuario"] = $this->usuarios->getById($this->session->userdata("id_usuario"));
		$this->load->view('datos_view', $data);	
	}

	public function edit() 
	{
		
		$this->form_validation->set_rules('cedula_usuario', 'Cédula', 'trim|numeric|required');
		$this->form_validation->set_rules('nombre_usuario', 'Nombres', 'trim|alpha_space|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|matches[password_confirmacion]|required');
		$this->form_validation->set_rules('password_confirmacion', 'Confirmar Password', 'trim|matches[password]|required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->index();
		}
		else
		{

			$usuario = array (	'id_usuario' => $this->session->userdata("id_usuario"),
								'cedula_usuario' => $_POST['cedula_usuario'],
								'nombre_usuario' => $_POST['nombre_usuario'],
								'login' => $_POST['cedula_usuario'],
								'password' => md5($_POST['password']),
								'password_confirmacion' => md5($_POST['password_confirmacion'])
						);
			$update_usuario = $this->usuarios->update($usuario);

			if ($update_usuario) {
				$data["mensaje"] = "Los datos se modificaron correctamente";				
			} else {
				$data['mensaje'] = 'Error: No se ha podido guardar';
			}
			$this->index($data);

		}

	}

}